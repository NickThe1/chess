package com.nick.software.chess.service;

import com.nick.software.chess.persistence.ChessBoard;
import com.nick.software.chess.persistence.Queen;

import java.util.HashSet;
import java.util.Set;

public class QueenConfigurerImpl implements QueenConfigurer{

    private ChessBoard chessBoard;
    private Queen queen;

    public QueenConfigurerImpl() {
        this.chessBoard = new ChessBoard();
    }

    public QueenConfigurerImpl(ChessBoard chessBoard, Queen queen) {
        this.chessBoard = chessBoard;
        this.queen = queen;
    }

    @Override
    public void setQueens(int y,int x) {

        Set<Integer> forbiddenSlopes = new HashSet<>();
        Set<Integer> forbiddenI = new HashSet<>();
        Set<Integer> forbiddenJ = new HashSet<>();
        int queenNumber = 1;
        int iter = 0;

        chessBoard.getBoard()[y][x] = 1;
        forbiddenSlopes.add(x - y);
        forbiddenI.add(y);
        forbiddenJ.add(x);

        for (int i = 0; i < chessBoard.getBoard().length; i++) {
            for (int j = 0; j < chessBoard.getBoard().length; j++) {
                if (!forbiddenSlopes.contains(j -i) && !forbiddenI.contains(i) && !forbiddenJ.contains(j)){
                    chessBoard.getBoard()[i][j] = 1;
                    forbiddenSlopes.add(j - i);
                    forbiddenI.add(i);
                    forbiddenJ.add(j);
                    queenNumber++;
                }
                iter++;
            }
        }
        if (queenNumber == 8) {
            System.out.println("number of reps " + iter + ", queen's amount " + queenNumber);
        }
    }

    @Override
    public void printChessBoard() {
        for (int i = 0; i < chessBoard.getBoard().length; i++) {
            for (int j = 0; j < chessBoard.getBoard().length; j++) {
                System.out.print(chessBoard.getBoard()[i][j] + " ");
            }
            System.out.println();
        }
    }
}
