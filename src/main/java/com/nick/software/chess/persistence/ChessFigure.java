package com.nick.software.chess.persistence;

public abstract class ChessFigure {
    //inner enum
    private final int VALUE;

    public ChessFigure(int value) {
        this.VALUE = value;
    }

    public int getVALUE() {
        return VALUE;
    }

}
