package com.nick.software.chess.service;

public interface QueenConfigurer {

    void setQueens(int y, int x);

    void printChessBoard();
}
