package com.nick.software.chess.persistence;

import java.util.List;

public class ChessBoard {

    private final int size = 8;
    private int[][] board = new int[size][size];
    private List<? extends ChessFigure> chessFigures;

    public ChessBoard() {
    }

    public ChessBoard(int[][] board, List<? extends ChessFigure> chessFigures) {
        this.board = board;
        this.chessFigures = chessFigures;
    }

    public int[][] getBoard() {
        return board;
    }

    public void setBoard(int[][] board) {
        this.board = board;
    }

    public List<? extends ChessFigure> getChessFigures() {
        return chessFigures;
    }

    public void setChessFigures(List<? extends ChessFigure> chessFigures) {
        this.chessFigures = chessFigures;
    }
}
