package com.nick.software.chess;

import com.nick.software.chess.service.QueenConfigurerImpl;

public class Main {
    public static void main(String[] args) {
        //QueenConfigurerImpl queenConfigurer = new QueenConfigurerImpl();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
               new QueenConfigurerImpl().setQueens(i, j);
            }
        }
    }
}
