package com.nick.software.chess.persistence;

public final class Queen  extends ChessFigure{

    public Queen(int value) {
        super(value);
    }
}
